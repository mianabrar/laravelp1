<!DOCTYPE html>
<html>
<head>
	<title>viewing all</title>
	  <link rel="stylesheet" type="text/css" href="styling.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<!-- Font family link-->
<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
<!--font awesome-->
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<h1 style="color: green;text-align: center;">Thanks for coming</h1>

	<h2 style="margin-top: 90px; text-align: center;margin-bottom: 60px;">Appointment List of All Patients

    <br>
 @auth
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                 @else
                                 <a href="/login">
                                            Login
                                        </a>
                                 @endif     
  </h2>
  @if(Auth::user()->role == "patient")
<small>Note: Dear user, your appointment is highlighted below</small>
@endif
<div class="table-responsive text-nowrap">

  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Patient Name</th>
        <th scope="col">Phone#</th>
        <th scope="col">Disease</th>
        <th scope="col">Gender</th>
        <th scope="col">Check-Up Time</th>
    @if(Auth::user()->role == "doctor")
        <th scope="col">Actions</th>
    @endif

      </tr>
    </thead>
    <tbody>
    @foreach($appointments as $appointment) 
      <tr 
@if($appointment->name == Auth::user()->name)
style="background:aqua"
@endif


       >
        <th scope="row">{{$appointment->id}}</th>
        <td>{{$appointment->name}}</td>
        <td>{{$appointment->phone}}</td>
        <td>{{$appointment->disease}}</td>
        <td>{{$appointment->gender}}</td>

<?php
$Appointment_date = $appointment->created_at;
$Appointment_date = date('Y-m-d H:i:s', strtotime($Appointment_date . ' +1 day'));


$date=date_create($Appointment_date);
$formattedAppDate = date_format($date,"d/m/Y g:ia");
?>
        <td>{{$formattedAppDate}}</td>
        @if(Auth::user()->role == "doctor")

        <td>
        <a href="appointment/{{$appointment->id}}/edit" type="button" class="btn btn-success">Edit
        </a>
        <a href="appointment/{{$appointment->id}}/delete" type="button" class="btn btn-danger">Delete
        </a>
      </td>
        @endif
      </tr>
@endforeach

    </tbody>
  </table>

</div>

</body>
</html>